# coding:utf-8
import os
import subprocess
import time
# import pandas as pd # 写入excel

testdir = os.getcwd()
casesdir = testdir[0: -4] + "Example"

if __name__ == '__main__':

    # try:
    #     raise Exception("raise my exception!!!")
    # except Exception as err:
    #     print('An exception happened: ' + str(err))

    print("Auto test begin...")
    print("test dir: " + testdir)
    print("cases dir: " + casesdir)
    print("*************************")

    casesNum = 0
    successNum = 0

    # 输出excel的格式
    filenames = []
    dirnames = []
    results = []
    errorcontent = []

    for root, dirs, files in os.walk(casesdir): # 遍历指定文件夹
        for myfile in files:
            if(myfile.endswith(".txt")):
                index = myfile.find('.')
                # case名称
                myfilename = myfile[0: index]
                suffix = myfile[index+1 : ]
                print("Testing: " + myfile)

                # filenames.append(myfilename)
                idx = root.find("Example") 
                idx_s = root.find("/",idx+8) 
                project = root[idx+8 : idx_s]
                # dirnames.append(project)

                # 准备打开子进程
                processfilepath = root + "/" + myfile
                filesize = os.path.getsize(processfilepath)   # KB
                print("filesize " + str(filesize))
                logfilepath = root + "/" + myfilename + "_" + suffix + "_GenHex.log"
                # mycmd = "../bin/hexahedronGen " + processfilepath # cmd命令
                mycmd = "./Add " + processfilepath # cmd命令

                # 打开子进程    
                processlogfile = open(logfilepath, 'w')
                process = subprocess.Popen(mycmd, shell=True, universal_newlines=True, stdout=processlogfile)
                starttime = time.time()
                endtime = 0.0

                fail = 0
                failinfo = ""

                # 监视子进程是否结束
                # 若超时则将其杀死
                while 1:
                    if subprocess.Popen.poll(process) != None:
                        endtime = time.time()
                        break
                    # tt = time.time()
                    # if (tt - starttime) > 1200:
                    #     failinfo = "timeout\n"
                    #     fail = 1
                    #     os.system("kill " + str(process.pid))
                    #     break

                try:
                    processlogfile.close()
                except:
                    pass

                myfiles = os.listdir(root)
                flag = 0
                for afile in myfiles:
                    if(afile.endswith("_GenHex.log")):
                        print("执行成功！")
                        break
                # if(flag == 0):
                #     fail = 1
                #     failinfo += "生成失败\n没错\n"

                # 检查是否遇到错误异常结束
                # if(fail == 0):
                #     logfile = open(logfilepath, 'r')
                #     info = logfile.readlines()
                #     line = info[-1]
                #     if line.find("finish writing .vtk file") != -1:
                #         logfile.close()
                #     else:
                #         for line in info:
                #             if line.find("WRONG") != -1 or line.find("wrong") != -1 or (line.find("err") != -1 and line.find("may occu") == -1) or line.find("ERR") != -1 or (line.find("Err") != -1 and line.find("invalid parent element") == -1) and line.find("point") == -1:
                #                 failinfo = line
                #                 fail = 1
                #                 break
                #         if(fail == 0):
                #             failinfo = "unknownerror\n"
                #             fail = 1
                        
                #         logfile.close()           

                # errorcontent.append(failinfo)  

                # 输出是否错误以及错误信息
                if fail == 1:
                    print(root + "/" + myfile + " fail " + failinfo.rstrip('\n'))
                    # results.append("fail")
                elif fail == 0:
                    print(root + "/" + myfile + " success time: " + str(round((endtime - starttime) / 60.0, 1)) + "min")
                    successNum = successNum + 1
                    # results.append("success")

                casesNum = casesNum + 1
                print("*************************")  

            else:
                continue
    
    # dfData = { # 用字典设置DataFrame所需数据
    #     '文件名': filenames,
    #     'Project': dirnames,
    #     '执行结果': results,
    #     '报错内容': errorcontent
    # }
    # df = pd.DataFrame(dfData)
    # df.to_excel("autotest.xlsx")

    # 打印测试结果
    print("casesnum: " + str(casesNum))
    print("successnum: " + str(successNum))

    # print("result failed")
    # raise Exception("raise my exception!!!") 