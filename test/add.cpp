#include <iostream>
#include <string>
#include <fstream>
#include <string.h>

int convert(char* input)
{
    int result = 0;
    int temp = 1;
    for(int i = strlen(input)-1 ; i >= 0 ;i--)
    { 
        result += (input[i]-'0')*temp;
        temp *= 10;
    }
    return result;
}

int main(int argc,char *argv[])
{
    std::string filename;

    filename = argv[1];
    
    std::ifstream readFile;

    int a,b;

    readFile.open(filename, std::ios::in);
    if(readFile.is_open())
    {
        char buff[1000] = {0};
        if (readFile >> buff)
        {
            a = convert(buff);
        }
        if(readFile >>buff)
        {
            b = convert(buff);
        }
    }
    std::cout << a+b << std::endl;
    return 0;
}
